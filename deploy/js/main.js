//*** ELEMEMENTOS 
var linhasClassificacao = $('tbody tr'),
	rodadas,
	listaJogos = $('#lista_jogos'),
	containerRodadas = $('#container_rodadas'),
	listaJogosArr,
	jogosEsqBtn = $('#nav_jogos_esq'),
	boxPontos = $('#box_pontos'),
	boxRebotes = $('#box_rebotes'),
	boxAssistencias = $('#box_assistencias'),
	navEsqBtn = $('#nav_jogos_esq'),
	navDirBtn = $('#nav_jogos_dir'),
	navTitulo = $('#titulo_rodada'),
	rodadaAtual,
	numRodadas,
	numRodadasPrimeiroTurno = 0,
	numRodadasSegundoTurno = 0,
	preloader = $('#preloader'),
	aprovBtn = $('#aprov_btn'),
	pBtn = $('#pBtn'),



//*** VARIÁVEIS
	liga = window.location.search,
	liga = liga.substr(liga.indexOf('=') + 1),
	liga = liga == "Masculina"? "M" : "F",
	// urlClassificacao = 'json/data_teste.json?id=' + Math.random() * 9999,
	urlClassificacao = 'data/classificacao'+ liga +'.xml?id=' + Math.random() * 9999,
	urlJogos = 'data/jogos'+ liga +'.xml?id=' + Math.random() * 9999,
	dadosClassificacao,
	dadosJogos,
	dados,
	dadosSortP,
	diasSemana = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];


//*** FUNÇÕES
function criaEventos(){
	navDirBtn.bind({'click':onClickNavJogos, 'mouseover' : onOverNavJogos});
	navEsqBtn.bind({'click':onClickNavJogos, 'mouseover' : onOverNavJogos});
}

function marcaColunas(col1, col2){
	$(col1).css({'font-weight':"bold", 'color' : 'black'});
	$(col2).css({'font-weight':"normal", 'color' : 'gray'})
}

function mostraRodada(numRodada){
	var turno,
		rodada;

	rodadas.each(rodadaHide);
	rodadaShow(numRodada);
	
	turno = (numRodada < numRodadasPrimeiroTurno) ? '1' : '2';

	rodada = (numRodada < numRodadasPrimeiroTurno) ? numRodada + 1 : numRodada - (numRodadasPrimeiroTurno) + 1;

	navTitulo.html(turno + "&ordm; turno - rodada " + rodada);
}

function rodadaHide(i){
	$(rodadas[i]).hide();
}

function rodadaShow(numRodada){
	$(rodadas[numRodada]).show();
}

function carregaDados(){
	$.ajax({url:urlClassificacao, }).done(onClassificacaoLoaded);
	$.ajax({url:urlJogos, }).done(onJogosLoaded);
}

function retiraPreloader(){
	preloader.hide();
}

function verificaDadosCarregados(){
	if(dadosClassificacao != undefined && dadosJogos != undefined){
		montaUI();
	}
}

function montaUI(){
	atualizaClassificacao();
	atualizaInfosJogos();
	criaEventos();
	retiraPreloader();
}

function atualizaClassificacao(){
	var classificacao = dadosClassificacao.getElementsByTagName('CLASS'),
		numTimes = classificacao.length,
		i,
		nome = $('.nome'),
		posicao = $('.classificacao_posicao'),
		col2 = $("tbody > tr > .col2"),
		col3 = $("tbody > tr > .col3"),
		col4 = $("tbody > tr > .col4"),
		col5 = $("tbody > tr > .col5");


	for(i=0; i<numTimes; i++){
		$(nome[i]).text(corrigeNome(classificacao[i].getElementsByTagName("NomeEq")[0].textContent));
		$(posicao[i]).text(i + 1);
		$(col2[i]).text(classificacao[i].getElementsByTagName("Pontos")[0].textContent);
		$(col3[i]).text(classificacao[i].getElementsByTagName("NumJogos")[0].textContent);
		$(col4[i]).text(classificacao[i].getElementsByTagName("NumVitorias")[0].textContent);
		$(col5[i]).text(classificacao[i].getElementsByTagName("NumDerrotas")[0].textContent);
	}

	if(liga == "F"){
		$(linhasClassificacao[10]).hide();
		$(linhasClassificacao[11]).hide()
	}
}

function atualizaInfosJogos(){
		lista_Jogos = dadosJogos.getElementsByTagName('jogos');

		var numJogosTotal = lista_Jogos.length,
			i,
			jogo,
			j,
			rodadaAtualmenteCriada = 0,
			turnoAtualmenteCriado = 0;

		containerRodadas.html('');
		

	for(j=0; j<lista_Jogos.length; j++){
	 	var rodada = parseInt(lista_Jogos[j].getElementsByTagName('rodada')[0].textContent),
	 		turno = parseInt(lista_Jogos[j].getElementsByTagName('turno')[0].textContent),
		 	
		 	htmlRodada = '<!-- TURNO '+ turno +' / RODADA '+ (rodadaAtualmenteCriada + 1 > 11 ? 1 : rodadaAtualmenteCriada + 1)+'-->'
					   // + 	'<div class="rodada" style="border:1px solid black">TURNO '+ turno +' / RODADA '+ (rodadaAtualmenteCriada + 1 > 11 ? 1 : rodadaAtualmenteCriada + 1) + '</div>';
						// + 	'<div class="rodada" style="border:1px solid black"></div>';
						+ 	'<div class="rodada"></div>';
		
			htmlJogo = 	"<div class='jogo_display'>"
                     + 	  "<!-- Mandante -->"
                     +	  "<span class='clube_mandante'>Vila Leopoldina</span>"
                     +	  "<span class='set'>0</span>"
                     +	  "<span class='col1'>0</span>"
                     +	  "<span class='col2'>0</span>"
                     +	  "<span class='col3'>0</span>"
                     +	  "<span class='col4'>0</span>"
                     +	  "<span class='col5'>0</span>"
                     
                     +	  "<!-- Visitante -->"
                     +	  "<span class='clube_visitante'>FLU</span>"
                     +	  "<span class='set  pad'>0</span>"
                     +	  "<span class='col1 pad'>0</span>"
                     +	  "<span class='col2 pad'>0</span>"
                     +	  "<span class='col3 pad'>0</span>"
                     +	  "<span class='col4 pad'>0</span>"
                     +	  "<span class='col5 pad'>0</span>"
                     
                     +	  "<!-- Infos -->"
                     +	  "<span class='jogo_infos'>Qua 19/01/2011 - 19h30 PedrocãoPedrocão</span>"
                  	 +  "</div>";

	 	if(rodada != rodadaAtualmenteCriada){
	 		if(rodadaAtualmenteCriada + 1 > 11){
		 		// console.log("criando rodada # 1");
		 		//containerRodadas.append(htmlRodada); // cria a div de rodadas
		 		rodadaAtualmenteCriada = 1;
		 		
		 	}else{
		 		// console.log("criando rodada #" + (rodadaAtualmenteCriada + 1));
		 		// containerRodadas.append(htmlRodada); // cria a div de rodadas
		 		rodadaAtualmenteCriada ++;
		 	}

			containerRodadas.append(htmlRodada); // cria a div de rodadas

		 	turno = lista_Jogos[j].getElementsByTagName('turno')[0].textContent
			if(turno == "1"){
				numRodadasPrimeiroTurno++;
			}else if(turno == "2"){
				numRodadasSegundoTurno++;
			}
	 	}

	 	rodada = $(".rodada");
		$(rodada[rodada.length - 1]).append(htmlJogo);

		var jogo = $($('.jogo_display')[j]),
		
			//Textfields
			clubeMandanteTxt = jogo.find('.clube_mandante'),
			resultadoMandanteTxt = $(jogo.find('.set')[0]),
			set1MandanteTxt = $(jogo.find('.col1')[0]),
			set2MandanteTxt = $(jogo.find('.col2')[0]),
			set3MandanteTxt = $(jogo.find('.col3')[0]),
			set4MandanteTxt = $(jogo.find('.col4')[0]),
			set5MandanteTxt = $(jogo.find('.col5')[0]),

			clubeVisitanteTxt = jogo.find('.clube_visitante'),
			resultadoVisitanteTxt = $(jogo.find('.set')[1]),
			set1VisitanteTxt = $(jogo.find('.col1')[1]),
			set2VisitanteTxt = $(jogo.find('.col2')[1]),
			set3VisitanteTxt = $(jogo.find('.col3')[1]),
			set4VisitanteTxt = $(jogo.find('.col4')[1]),
			set5VisitanteTxt = $(jogo.find('.col5')[1]),

			infosTxt = $(jogo.find('.jogo_infos')),


			//Valores
			clubeMandanteStr = lista_Jogos[j].getElementsByTagName('timemandante')[0].textContent,
			clubeVisitanteStr = lista_Jogos[j].getElementsByTagName('timevisitante')[0].textContent,
			
			setStr = lista_Jogos[j].getElementsByTagName('resultado')[0].textContent,
			resultadoMandanteStr = setStr.substring(0, 1),
			resultadoVisitanteStr = setStr.substr(setStr.indexOf("x") + 1, 2),
			
			setsMandanteStr = lista_Jogos[j].getElementsByTagName('setsmandante')[0].textContent,
			set1MandanteStr = setsMandanteStr.substr(0,2),
			set2MandanteStr = setsMandanteStr.substr(11,2),
			set3MandanteStr = setsMandanteStr.substr(22,2),
			set4MandanteStr = setsMandanteStr.substr(33,2),
			set5MandanteStr = setsMandanteStr.substr(44,2),

			setsVisitanteStr = lista_Jogos[j].getElementsByTagName('setsvisitante')[0].textContent,
			set1VisitanteStr = setsVisitanteStr.substr(0,2),
			set2VisitanteStr = setsVisitanteStr.substr(11,2),
			set3VisitanteStr = setsVisitanteStr.substr(22,2),
			set4VisitanteStr = setsVisitanteStr.substr(33,2),
			set5VisitanteStr = setsVisitanteStr.substr(44,2),

			dataFull = lista_Jogos[j].getElementsByTagName('data')[0].textContent;
			data = dataFull.substring(0, dataFull.indexOf("-"));
			hora = lista_Jogos[j].getElementsByTagName('hora')[0].textContent;
			// data = new Date(ano, (mes-1), dia);
			// diaSemana = diasSemana[data.getDay()],
			diaSemana = corrigeDiaSemana(dataFull.substring(dataFull.indexOf("(")+1, dataFull.lastIndexOf(")")));
			ginasio = lista_Jogos[j].getElementsByTagName('ginasio')[0].textContent;
			cidade = lista_Jogos[j].getElementsByTagName('cidade')[0].textContent;

			// infosTxt.text(diaSemana + " " + dia +"/"+ mes +"/"+ ano + " - " + hora + " - " +  cidade + " - " + ginasio);
			infosTxt.text(diaSemana + " " + data + " - " + hora + " - " +  cidade + " - " + ginasio);

		//Atribuições
		clubeMandanteTxt.text(corrigeNome(clubeMandanteStr));
		resultadoMandanteTxt.text(resultadoMandanteStr);
		set1MandanteTxt.text(set1MandanteStr);
		set2MandanteTxt.text(set2MandanteStr);
		set3MandanteTxt.text(set3MandanteStr);
		set4MandanteTxt.text(set4MandanteStr);
		set5MandanteTxt.text(set5MandanteStr);

		clubeVisitanteTxt.text(corrigeNome(clubeVisitanteStr));
		resultadoVisitanteTxt.text(resultadoVisitanteStr);
		set1VisitanteTxt.text(set1VisitanteStr);
		set2VisitanteTxt.text(set2VisitanteStr);
		set3VisitanteTxt.text(set3VisitanteStr);
		set4VisitanteTxt.text(set4VisitanteStr);
		set5VisitanteTxt.text(set5VisitanteStr);

		
		//Define rodada atual
		if(setsMandanteStr == ' ' && !rodadaAtual){
			rodadaAtual = parseInt(lista_Jogos[j].getElementsByTagName('rodada')[0].textContent) - 1;
			// rodadaAtual = 0;
			// console.log(rodadaAtual);
		}
		
		
		//console.log(setsVisitanteStr);
	}

	rodadas = $('.rodada');
	numRodadas = rodadas.length;

	onJogosCriados();
}

function corrigeDiaSemana(nomeStr){
	var nome;
	// console.log(nomeStr);
	switch(nomeStr){
		case "dom":
			nome = diasSemana[0];
			break;

		case "seg":
			nome = diasSemana[1];
			break;

		case "ter":
			nome = diasSemana[2];
			break;

		case "qua":
			nome = diasSemana[3];
			break;

		case "qui":
			nome = diasSemana[4];
			break;

		case "sex":
			nome = diasSemana[5];
			break;

		case "sáb":
			nome = diasSemana[6];
			break;
	}

	return nome;
}

function corrigeNome(nomeStr){
	var nome;

	switch(nomeStr){
		// Masculino
		case "SADA CRUZEIRO":
			nome = "Cruzeiro";
			break;

		case "SADA CRUZEIRO VOLEI":
			nome = "Cruzeiro";
			break;

		case "RJX":
			nome = "Rio de Janeiro";
			break;

		case "VIVO/MINAS":
			nome = "Minas";
			break;

		case "SAO BERNARDO VOLEI":
			nome = "São Bernardo";
			break;

		case "SÃO BERNARDO VÔLEI" :
			nome = "São Bernardo";
			break;

		case "CANOAS":
			nome = "Canoas";
			break;

		case "MEDLEY/CAMPINAS":
			nome = "Campinas";
			break;

		case "VOLEI FUTURO":
			nome = "Vôlei Futuro";
			break;

		case " SESI-SP":
			nome = "Sesi";
			break;

		case "SUPER IMPERATIZ VOLEI":
			nome = "Florianópolis";
			break;

		case "SUPER IMPERATRIZ VÔLEI":
			nome = "Florianópolis";
			break;

		case "FUNVIC/MIDIA FONE":
			nome = "Pindamonhangaba";
			break;

		case "VOLTA REDONDA":
			nome = "Volta Redonda";
			break;

		case "UFJF":
			nome = "Juiz de Fora";
			break;

		// Feminino

		case "SÃO CRISTÓVÃO SAÚDE/SÃO CAETANO":
			nome = "São Caetano";
			break;

		case "S.CRISTOVAO SAUDE/S.C.":
			nome = "São Caetano";
			break;	

		case "SESI-SP":
			nome = "Sesi";
			break;
		
		case "E.C. PINHEIROS":
			nome = "Pinheiros";
			break;
		
		case "UNILEVER":
			nome = "Rio de Janeiro";
			break;
		
		case "SOLLYS/NESTLÉ":
			nome = "Osasco";
			break;
		
		case "SOLLYS/NESTLE":
			nome = "Osasco";
			break;

		case "BANANA BOAT/PRAIA CLUBE":
			nome = "Praia Clube";
			break;
		
		case "RIO DO SUL":
			nome = "Rio do Sul";
			break;
		
		case "USIMINAS/MINAS":
			nome = "Minas";
			break;
		
		case "VOLEI AMIL":
			nome = "Campinas";
			break;

		default:
			nome = nomeStr;
	}
	return nome;
}




//*** CALLBACKs
function onClassificacaoLoaded(e){
	dadosClassificacao = e;

	verificaDadosCarregados();
}

function onJogosLoaded(e){
	dadosJogos = e;

	verificaDadosCarregados();
}

function onClickNavJogos(e){
	var bt = $(e.target);

	switch(bt.attr('id')){
		case 'nav_jogos_esq':
			if(rodadaAtual > 0)
				rodadaAtual--;
			break;

		case 'nav_jogos_dir':
			if(rodadaAtual <= numRodadas - 2)
				rodadaAtual++;
				// habilita(navEsqBtn);
	}

	mostraRodada(rodadaAtual);
	
	if(rodadaAtual == 0){
		desabilita(navEsqBtn);
	}
	else if(rodadaAtual == numRodadas - 1){
		desabilita(navDirBtn);
	}
	else{
		habilita(navEsqBtn);
		habilita(navDirBtn);
	}
}

function habilita(btn){
	btn.css("background-position-x" , "0px");
}

function desabilita(btn){
	btn.css("background-position-x" , "-35px");
}

function onOverNavJogos(e){
	var bt = $(e.target);
}

function onJogosCriados(){
	mostraRodada(rodadaAtual);
}

//*** RUN
carregaDados();

if(rodadaAtual == 0)
{
	desabilita(navEsqBtn);
}


