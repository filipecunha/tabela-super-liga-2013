<!doctype html>
<html>
<head>
   <meta charset="utf-8">
   <title>Tabela Superliga</title>
   <link rel="stylesheet" href="css/main.css">
</head>
<body>
   <div id='preloader'><img src="img/preloader.gif"></div>
   <h1>Superliga <? echo $_GET['liga']?></h1>
   <!-- CONTAINER DE CLASSIFICAÇÃO E JOGOS -->
   <div id="container">
      <div id="container_classificacao">
         <!-- TABELA CLASSIFICAÇÃO -->
         <h2 class='invisible'>Tabela de classificação</h2>
         <table>
            <thead >
               <tr>
                  <td class='col1'>CLASSIFICA&Ccedil;&Atilde;O</td>
                  <td class='col2'>P</td>
                  <td class='col3'>J</td>
                  <td class='col4'>V</td>
                  <td class='col5'>D</td>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
               <tr>
                  <td class='col1'><span class="classificacao_posicao">1</span> <span class='nome'>Nome do Clube</span></td>
                  <td class='col2'>2</td> 
                  <td class='col3'>3</td>
                  <td class='col4'>4</td>
                  <td class='col5'>5</td>
               </tr>
            </tbody>
         </table>
      </div>
      
      <!-- CONTAINER DA LISTA DE JOGOS -->
      <div id="container_jogos">
         <!-- TABELA DE JOGOS -->
         <div id='lista_jogos'>
            <!-- NAVEGAÇÃO -->
            <div id='nav_jogos'>
               <!-- <img id="nav_jogos_esq" src="img/btOff_esq.gif" /> -->
               <a id='nav_jogos_esq'></a>
               <span id="titulo_rodada">1º turno - rodada 1</span> 
               <!-- <img id="nav_jogos_dir" src="img/btOff_dir.gif" /> -->
               <a id='nav_jogos_dir'></a>
            </div>
            <div id='legenda_jogos'>
               <span></span>
               <span id='espacamento'></span>
               <span id='set'>SET</span>
               <span class='col1'>1</span>
               <span class='col2'>2</span>
               <span class='col3'>3</span>
               <span class='col4'>4</span>
               <span class='col5'>5</span>
            </div>

            <!-- LISTA DE JOGOS -->
            <div id='container_rodadas'>
               <!-- RODADA1 -->
               <div class='rodada'>
                  <div class='jogo_display'>
                     <!-- Mandante -->
                     <span class="clube_mandante">Vila Leopoldina</span>
                     <span class='set'>0</span>
                     <span class='col1'>0</span>
                     <span class='col2'>0</span>
                     <span class='col3'>0</span>
                     <span class='col4'>0</span>
                     <span class='col5'>0</span>
                     
                     <!-- Visitante -->
                     <span class="clube_visitante">FLU</span>
                     <span class='set  pad'>0</span>
                     <span class='col1 pad'>0</span>
                     <span class='col2 pad'>0</span>
                     <span class='col3 pad'>0</span>
                     <span class='col4 pad'>0</span>
                     <span class='col5 pad'>0</span>
                     
                     <!-- Infos -->
                     <span class="jogo_infos">Qua 19/01/2011 - 19h30 PedrocãoPedrocão</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- DISCLAIMER -->
   <div id="disclaimer">Dados cedidos pelo site <a href="http://www.cbv.com.br">cbv.com.br</a></div>
      
   <!-- LEGENDA -->  
   <div id="legenda"><strong>P</strong> Pontos    <strong>J</strong> Jogos     <strong>V</strong> Vit&oacute;rias    <strong>D</strong> Derrotas</div>
   
   <!-- jQuery -->
   <script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
   
   <!-- main.js -->
   <script src="js/main.js" type="text/javascript"></script>
</body>
</html>

