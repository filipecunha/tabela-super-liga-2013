<?

function clonaFile($from, $to)
{
	// Lê o arquivo $from
	$ch = curl_init($from);
	//curl_setopt($ch, CURLOPT_PROXY, "http://proxy01.globoi.com:3128");
	curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    $str = curl_exec($ch);
    curl_close($ch);

	// Escreve no $to
	$fh = fopen($to, 'w') or die("Não foi possível abrir o arquivo");

	if(fwrite($fh, $str))
		return true;
	else
		return false;
	
	fclose($fh);
}
// atualiza dados da liga masculina
$file1 = clonaFile('http://www.superliga.com.br/rank.asp?trp=n&fm=m', "../data/classificacaoM.xml");
$file2 = clonaFile('http://www.superliga.com.br/tabeladejogos.asp?fm=m', "../data/jogosM.xml");

// atualiza dados da liga feminina
$file3 = clonaFile('http://www.superliga.com.br/rank.asp?trp=n&fm=f', "../data/classificacaoF.xml");
$file4 = clonaFile('http://www.superliga.com.br/tabeladejogos.asp?fm=f', "../data/jogosF.xml");


if($file1 && $file2 && $file3 && $file4)
	echo "ok";
else
	echo "erro";
?>